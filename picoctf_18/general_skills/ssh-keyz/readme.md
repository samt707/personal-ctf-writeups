# picoCTF 2018- ssh-keyz (General Skills, 150 Points)

## Overview 

You are told the following:

```
As nice as it is to use our webshell, sometimes its helpful to connect directly to our machine. To do so, please add your own public key to ~/.ssh/authorized_keys, using the webshell. The flag is in the ssh banner which will be displayed when you login remotely with ssh to with your username. 
```

## Setting up Public/Private Key-Pair

First, run the "ssh-keygen" command on your local host to generate a public-private key-pair. If you already have one, there is no need to run this command.
You can go ahead and press the ENTER key through all the questions or options in order to just use defaults, or you can add a password, change location, etc. if you'd like.

```bash
ssh-keygen

```

![ssh](picoctf_18/general_skills/ssh-keyz/img/ssh1.png)

## Copying Public Key to Remote Host

Now, we need to get our public key on the remote host. Go ahead and print out your public key and copy it to your clipboard.

```bash
cat ~/.ssh/id_rsa.pub
```

![ssh](picoctf_18/general_skills/ssh-keyz/img/ssh2.png)

Next, access the web shell on picoCTF's website and log in.

![ssh](picoctf_18/general_skills/ssh-keyz/img/ssh3.png)

Now, we have to make a directory named "~/.ssh" and create a file in it called "authorized keys". Once we made the file, we need to copy and paste our public key into that file.

```bash
mkdir ~/.ssh
touch ~/.ssh/authorized_keys
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC/mwmWZY+8t1Rnla5oW3JChoTZLAM30QFncJtJgbc5em6LfWhKdFfdi3m/K3e+xDglvjejpfCV6atRLFQEOr0MeOCVqSjl0G21oIelgeTID7i7rZfS2OneUql2G2e0yaWceC12dDOA87Y8OuAIl3cXfTaSHF5A7GKsqhLPu1M8jbtvW/YJZeV0sJE70UeuQJjBgz+VXNYsSzuaYBCHrYVzoAfYIk6xAJzEnUadMsaT7mYrQYMyv4F3dHnfy/sdKQLkC7S1nIwFGq0PNhirlysJpyQFjLMa9HX/l6ImhqVmmdlilNdfWK17xHZhlv9TsKiZUvd+8WvSyvxO+rm4v7th localadmin@parrot" > ~/.ssh/authorized_keys
```
![ssh](picoctf_18/general_skills/ssh-keyz/img/ssh4.png)


Now, our public key should be on the remote host. So if everything worked correctly, we should be able to login with out picoCTF username and password.

```bash
ssh komrad@2018shell2.picoctf.com
```

![ssh](picoctf_18/general_skills/ssh-keyz/img/ssh5.png)

And there is the flag.

```
picoCTF{who_n33ds_p4ssw0rds_38dj21}
```
