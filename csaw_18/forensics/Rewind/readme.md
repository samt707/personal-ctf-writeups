# CSAW 18 Quals- Rewind (Forensics, 200 Points)

## Overview 

You are given a tar file and told:

```
Sometimes you have to look back and replay what has been done right and wrong

```

# Initial Forensics 

I first start by running "tar" on the file to extract the contents.

![cat file](csaw_18/forensics/Rewind/img/rewind1.png)

We then get a .zip file out, so let's unzip it.

![cat file](csaw_18/forensics/Rewind/img/rewind2.png)


Now, we have two files. We run the "file" command on them and see that one is just a log file, and the other is a suspended QEMU image.

![cat file](csaw_18/forensics/Rewind/img/rewind3.png)


Let's look through the log file first using the "strings" command.

# Strings

Since we know the flag format, let's just grep for "flag{" on the log file.

![cat file](csaw_18/forensics/Rewind/img/rewind4.png)


No returns. 

Let's try the same on the other file.

![cat file](csaw_18/forensics/Rewind/img/rewind5.png)


We get alot of returns. Let's assume that the one that says "FAKE" is not the flag. Let's try the other one.


It works!

```
flag{RUN_R3C0RD_ANA1YZ3_R3P3AT}
```
