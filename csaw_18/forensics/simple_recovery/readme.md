# CSAW 18 Quals- simple_recovery (Forensics, 150 Points)

## Overview 

You are given two .7z files and told:

```
Simple Recovery Try to recover the data from these RAID 5 images!
```

## Intial Forensics

I first started off by downloading the two files and then extracting the contents.

![cat file](csaw_18/forensics/simple_recovery/img/simple1.png)

![cat file](csaw_18/forensics/simple_recovery/img/simple2.png)

![cat file](csaw_18/forensics/simple_recovery/img/simple3.png)

After that, I ran the "file" command to figure out what kind of files they were.

![cat file](csaw_18/forensics/simple_recovery/img/simple4.png)


I haven't seen these kinds of files before, so the first thing I usually do with any type of file forensics problem is I run the "strings" command on the file.



## Strings

We start off by running "strings" on the first file.

![cat file](csaw_18/forensics/simple_recovery/img/simple5.png)

Let's narrow our search a litte and "grep" for "flag".

![cat file](csaw_18/forensics/simple_recovery/img/simple6.png)


There it is!

```
flag{dis_week_evry_week_dnt_be_securty_weak}

```
