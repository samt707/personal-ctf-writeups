# ISITDTU CTF 2018 Quals- Drill (Misc)

## Overview

You are first given a file called "Drill". There is no other hints or guidance given. 

## Inital Forensics 

I first start by running 'cat' on the file.

```bash
cat drill
```
![cat file](isitdtu_ctf_18/misc/drill/img/catdrill.png)


## Fixing The Zip File

It looks like the ASCII in the file is actually the hex of a binary. Let's do some xxd magic to convert the hexdump into a binary file, and then run xxd on it and see what it looks like.

```bash
xxd -r -p drill > newdrill 
xxd newdrill
```
![xxd newfile](isitdtu_ctf_18/misc/drill/img/xxdnewdrill.png)

After running xxd on the new file we just created, the file header seems to be that of a zip file, but the first couple bytes are missing. Let's add those in:

```bash
printf "\x50\x4b\x03\x04" | cat - newdrill > drill.zip
```

Now we should be able to unzip the file

```bash
unzip drill.zip
```
![unzip](isitdtu_ctf_18/misc/drill/img/zipneedspassword.png)


Oh no...It's password protected.


## Cracking The Zip


Luckly, we can use fcrackzip with rockyou.txt to crack the zip. 


```bash
fcrackzip -b -D -p /usr/share/wordlists/rockyou.txt -u drill.zip
```
![crack](isitdtu_ctf_18/misc/drill/img/passwordworked.png)

Awesome! We got the password, it's "brandon1". Let's go ahead and put that in. 

It worked! But look at the file that we got out of it... "499.zip".
I'm no expert, but it seems to me we have to unzip 499 more files to get to the flag. Let script this up real quick....


## Python Script

![script](isitdtu_ctf_18/misc/drill/img/python_script.png)


Let's run it and see what happens...

![worked](isitdtu_ctf_18/misc/drill/img/Ithinkitworked.png)


I think it worked. We are now left with 0.zip, let's unzip it. 


Luckily no password required! But we now have two files:
- key.png
- box.zip

When you try to unzip box.zip, it requires a password, but fcrackzip does not work on it. The password is probably in key.png. 

## Morse Code

When you open up key.png, you won't see anything at first glance, but if you look closely in the top right, there are 95 red-colored pixels.

![key.png](isitdtu_ctf_18/misc/drill/img/key.png)

I used gimp to zoom in. 

![gimp](isitdtu_ctf_18/misc/drill/img/gimpkep.png)

If you try converting them to binary, you won't get any readable text. Let's try Morse code. 

So you can't actually make out the morse code visually, the dashes (-) (decimal 45) and dots (.) (decimal 46) are encoded in the lighter red pixels.
There are actually three different shades of red, but you can't make them out with the naked eye, you can only see two. The darker red has a space encoded in it (decimal 32).
When I say encoded, I really mean the RGB value of the color is either 45, 46, or 32.
For example, the first pixel in the top-left will have a RGB value of (45,0,0). This is what it looks like when you use the color picker tool in gimp and click on the first pixel.
Note the value next to the 'R' (Red); it's 45, or a dash (-).
![45](isitdtu_ctf_18/misc/drill/img/gimp45.png)

Now let's look at the second pixel, it has a R value of 46, or a dot (.).

![46](isitdtu_ctf_18/misc/drill/img/gimp46.png)

And finally, the "darker" red pixels, have a R value of 32, or a space.

![32](isitdtu_ctf_18/misc/drill/img/gimp32.png)

So you could go through and get the value of each pixel and translate that to morse, or you can write a script.
I don't know about you, but I'm going to script it up. 

![moresescript](isitdtu_ctf_18/misc/drill/img/morse.png)

Let's see if it works...

![morse](isitdtu_ctf_18/misc/drill/img/morsecode.png)

```
-.- . -.-- .. ... --- -. .-.. -.-- ..-. --- .-. .-.. ..- -.-. -.- -.-- .... ..- -. - . .-. ... 
```



Now that you have the morse, all you have to do is translate it to ASCII. I used an online decoder.

![decoder](isitdtu_ctf_18/misc/drill/img/moresetoascii.png)

```
KEYISONLYFORLUCKYHUNTERS
```

Go ahead and convert that to lower case, and that should be the password for box.zip.

```
keyisonlyforluckyhunters
```

Once you unzip it, you should get a file name 'flag.txt'.

Once you cat it, you should get the flag.

![flag](isitdtu_ctf_18/misc/drill/img/flag.png)

```
ISITDTU{4_g00d_hunt3r_0n_th3_c0mput3r!}
```

