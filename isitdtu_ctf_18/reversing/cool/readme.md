# ISITDTU CTF 2018 Quals- Cool (Reversing)

## Overview

You are given a binary and told nothing else.

## Inital Forensics

When you run 'file' on the given file, you are told it's a ELF 64-bit LSB exectubale, x86-64.

Next, I run strings on it, and find something very interesting...

![strings](isitdtu_ctf_18/reversing/cool/img/strings.png)

They look like MD5 hashes. After putting them through an online MD5 dictionary, you get the following strings:

<img src="isitdtu_ctf_18/reversing/cool/img/hash1.png" width="1000" height="150">
<img src="isitdtu_ctf_18/reversing/cool/img/hash2.png" width="1000" height="150">
<img src="isitdtu_ctf_18/reversing/cool/img/hash3.png" width="1000" height="150">


```
ECFD4245812B86AB2A878CA8CB1200F9 = fl4g
88E3E2EDB64D39698A2CC0A08588B5FD = _i5_
BBC86F9D0B90B9B08D1256B4EF76354B = h3r3

```
There is really nothing else to gain from strings, so lets try to run the binary to see what happens.

![cool](isitdtu_ctf_18/reversing/cool/img/runningcool.png)



It looks like it just asks for the key. Let's see what we can get from Binary Ninja.

## Binary Ninja

The first thing we notice in Binary Ninja is that the first check that the binary does is to see if the length of the key is 0x1c, or 28.

![bin1](isitdtu_ctf_18/reversing/cool/img/binaryninja28.png)


After that, it looks like the program checks 4 bytes at a time if our input is equal to the the three hashes that we found earlier. So in theory, we should have about half of the flag.

![bin2](isitdtu_ctf_18/reversing/cool/img/hash1check.png)

![bin3](isitdtu_ctf_18/reversing/cool/img/hash2check.png)

![bin4](isitdtu_ctf_18/reversing/cool/img/hash3check.png)


After checking the first 12 bytes, it checks to see if the thirteenth byte is 0x21, or a exclamation point (!)

![bin4](isitdtu_ctf_18/reversing/cool/img/!.png)


So far, we know the key is:

```
fl4g_i5_h3r3!
```

The next part is not as straight forward, so we are going to decompile the binary using IDA.

## IDA

Here is the most important part of the decompiled code (this is where the actual check happens). The full decompiled code will be in the 'scripts_files' folder.

```c

__int64 __fastcall main(__int64 a1, char **a2, char **a3)
{
  __int64 result; // rax@19
  __int64 v4; // rcx@19
  unsigned __int8 v5; // [rsp+7h] [rbp-69h]@12
  signed int i; // [rsp+8h] [rbp-68h]@11
  signed int j; // [rsp+Ch] [rbp-64h]@12
  int s; // [rsp+10h] [rbp-60h]@3
  __int64 v9; // [rsp+68h] [rbp-8h]@1

  v9 = *MK_FP(__FS__, 40LL);
  sub_400ADE();
  printf("Give me your key: ", a2);
  sub_400B21((unsigned __int8 *)dword_602100, 0x30u);
  sub_400BDE();
  if ( strlen(dword_602100) != 28 )
    sub_400D62();
  memset(&s, 0, 8uLL);
  s = *(_DWORD *)dword_602100;
  if ( !(unsigned int)sub_400CE5((const char *)&s, "ECFD4245812B86AB2A878CA8CB1200F9") )
    sub_400D62();
  memset(&s, 0, 8uLL);
  s = dword_602104;
  if ( !(unsigned int)sub_400CE5((const char *)&s, "88E3E2EDB64D39698A2CC0A08588B5FD") )
    sub_400D62();
  memset(&s, 0, 8uLL);
  s = dword_602108;
  if ( !(unsigned int)sub_400CE5((const char *)&s, "BBC86F9D0B90B9B08D1256B4EF76354B") )
    sub_400D62();
  if ( byte_60210C != 33 )
    sub_400D62();
  for ( i = 13; dword_602100[(signed __int64)i]; ++i )
  {
    v5 = 0;
    for ( j = 0; j <= i; ++j )
      v5 ^= dword_602100[(signed __int64)j];
    if ( v5 != byte_6020A8[i - 13] )
      sub_400D62();
  }
  puts("Congratulation~~~");
  printf("Your flag: ISITDTU{%s}\n", dword_602100);
  result = 0LL;
  v4 = *MK_FP(__FS__, 40LL) ^ v9;
  return result;
}

```

We already got the first part of the key, so let's focus in on the last loop and the character array.

```c
char byte_6020A8[15] =
{
  '}',
  'M',
  '#',
  'D',
  '6',
  '\x02',
  'v',
  '\x03',
  'o',
  '[',
  '/',
  'F',
  'v',
  '\x18',
  '9'
};
 for ( i = 13; dword_602100[(signed __int64)i]; ++i )
  {
    v5 = 0;
    for ( j = 0; j <= i; ++j )
      v5 ^= dword_602100[(signed __int64)j];
    if ( v5 != byte_6020A8[i - 13] )
      sub_400D62();
  }
```
## Python Script


So what this loop is doing is going through and bitwise-XORing each character of the key and making sure that the result is equal to its respective value in the character array.
For example, for the first iteration of the loop, it is going to loop over each of the 14 characters of our flag (fl4g_i5_h3r3!), and bitwise-XOR the first with second, and then that result with the third character, etc.
Since i=13, it is also going to check the 14th character in our key (which we don't know yet.). What we do know, is what the XOR operation should equal in the end ('}') from our character array. 
So what we can do, since XOR is commutative, we can work backwards (or in any order for that matter) and get what our 14th character should be equal to. 
We are going to XOR '}' with !, and then that result with '3', and that result with 'r' and so on. The answer should give us what our 14th character should be. 
All we have to do now is loop over the rest of key and do the same thing. 

![script](isitdtu_ctf_18/reversing/cool/img/crackscript.png)


When we run the script, we get the following key:


![key](isitdtu_ctf_18/reversing/cool/img/key.png)
```
fl4g_i5_h3r3!C0ngr4tul4ti0n!

```

That looks right, but let's run the program just to verify

![flag](isitdtu_ctf_18/reversing/cool/img/flag.png)

```
Your flag: ISITDTU{fl4g_i5_h3r3!C0ngr4tul4ti0n!}

```
