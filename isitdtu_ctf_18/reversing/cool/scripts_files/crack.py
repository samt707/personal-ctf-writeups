#!/usr/bin/python3
#--------------------------------------------------
#File Name: crack.py
#
#Creation Date: 28-07-2018
#
#Last Modified Date: Sat 28 Jul 2018 11:15:13 PM EDT
#
#Author: Your Name Here
#
#Class:
#
#Assignment: reverse
#--------------------------------------------------


str1 = "fl4g_i5_h3r3!" #len=13 (0-12)

hexarr=['}','M', '#', 'D', '6', '\x02', 'v', '\x03','o', '[', '/', 'F', 'v', '\x18', '9']
y=0
while y < 15:
    x=len(str1)-1
    temp=int(ord(str(str1[x]))^ord(str(hexarr[y])))
    while x > 0:
        x=x-1
        temp=temp^ord(str(str1[x]))
    str1=str1+str(chr(temp))
    y=y+1
print(str1)

