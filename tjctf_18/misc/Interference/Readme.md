# TJCTF 2018- Interference (Misc, 15 Points)

## Overview


You are given a zip file and told the following:


```
I was looking at some images but I couldn't see them clearly. I think there's some interference.
```

## Initial Forensics

Since it is a zip file, let's start by unzipping it.

![unzip](tjctf_18/misc/Interference/img/inter1.png)

We get two images:

- v1.png
- v2.png

Let's look at both of them.

![unzip](tjctf_18/misc/Interference/img/inter2.png)

![unzip](tjctf_18/misc/Interference/img/inter3.png)

## Stegsolve

Let's go ahead and run stegsolve on both of them. Running stegsolve on v1.png we get nothing.

![unzip](tjctf_18/misc/Interference/img/inter4.png)

Running stegsolve on v2.png, we get a QR code. 

![unzip](tjctf_18/misc/Interference/img/inter5.png)

I downloaded a QR code scanner on my phone and scanned the code.

![unzip](tjctf_18/misc/Interference/img/inter6.jpg)

Here is what I got:

![unzip](tjctf_18/misc/Interference/img/inter7.jpg)

```
tjctf{m1x1ing_and_m4tchIng_1m4g3s_15_fun}

```