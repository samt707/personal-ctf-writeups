# TJCTF 2018- Nothing but Everything (Misc, 30 Points)

## Overview

You are given a .tar.gz file and told the following:

```
My computer got infected with ransomware and now none of my documents are accessible anymore! If you help me out, I'll reward you a flag!
```

## Inital Forensics

When we untar the file, we see a file with the following name:

```
1262404985085867488371
```


In that directory, there seems to be more like-named files and directories, as well as "HAHAHA.txt" which says:

```
All your files have been encrypted with a uncrackable algorithm.
You are now screwed forever.
```

![tar](tjctf_18/misc/NothingbutEverything/img/tar1.png)

If you cat any of the files, you just get a very long string of numbers. 

![tar](tjctf_18/misc/NothingbutEverything/img/tar2.png)


As you go through all the files and directories, you see that they are all pretty much at same format. Since we got no type of key or anything, it probably means that the decimal values just represent the text in some way.
Time for python.

## Python

Let's try first taking the first directory "1262404985085867488371" and see what we can do with it. Maybe if we convert it to hex, we'll get something promising.

![tar](tjctf_18/misc/NothingbutEverything/img/tar3.png)

In hex, we get "0x446f63756d656e7473".

When we convert this to ASCII, we get "Documents"!
We got it, now we just have to find the right file.

I decided to write a basic python script that does 3 main functions:
- Renames files
- Renames Directories 
- "Unencrypt" files (really just convert the large decimal to hex and write that to a file)

![tar](tjctf_18/misc/NothingbutEverything/img/tar4.png)

Let's start by renaming the two directories and all files in the "Documents" directory.

![tar](tjctf_18/misc/NothingbutEverything/img/tar5.png)


Doesn't look like any of the files in the directory would have the flag. Let's move to the "Work" directory and do the same thing.

![tar](tjctf_18/misc/NothingbutEverything/img/tar6.png)

Look's like there are two files named "here.xlsx" and "here2.xlsx". Let's go ahead and run it through the python script so we can change the contents of the two files from a long deciaml number to raw hex.
NOTE: the script cannot handle file names with spaces, so I had to manually rename two of the files in the "Work" directory.

![tar](tjctf_18/misc/NothingbutEverything/img/tar7.png)

Now we have two new files that we can open. Let's start with the first one. 

![tar](tjctf_18/misc/NothingbutEverything/img/tar8.png)

Doesn't seem to be anything in the first one. Let's try the second one.

![tar](tjctf_18/misc/NothingbutEverything/img/tar9.png)


There it is! The flag is the name of the sheet in the lower left corner. 

```
tjctf{n00b_h4x0r_b357_qu17}

```
