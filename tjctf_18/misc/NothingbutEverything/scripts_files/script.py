#!/usr/bin/python3
#--------------------------------------------------
#File Name: script.py
#
#Creation Date: 09-08-2018
#
#Last Modified Date: Thu 09 Aug 2018 01:21:16 PM EDT
#
#Author: Your Name Here
#
#Class:
#
#Assignment: nothing_but_everything
#--------------------------------------------------
import sys
import binascii
import os
#f=file
#d=dir
#w=write (outputs new file called NEW_filename)
#Usage=./script.py f file_name
#Usage=./script.py d directory_name
#Usage=./script.y w file_name
flag=sys.argv[1]
name=sys.argv[2]
if flag=="w":
    file1=open(name, "r+")
    dec1=file1.read()
    dec1_hex=hex(int(dec1))
    contents=bytearray.fromhex(dec1_hex[2:])
    newName="NEW_"+name
    new_file=open(newName, "wb")
    new_file.write(contents)
if flag=="f":
    name_hex=hex(int(name))
    name_ascii=bytearray.fromhex(name_hex[2:]).decode()
    print("mv " + name + " " + name_ascii)
    os.system("mv " + name + " " + name_ascii)
if flag=="d":
    name_hex=hex(int(name[:-1]))
    name_ascii=bytearray.fromhex(name_hex[2:]).decode()
    os.system("mv " + name + " "+ name_ascii + "/")
