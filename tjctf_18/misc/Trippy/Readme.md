# TJCTF 2018- Trippy (Misc, 5 Points)

## Overview

You are given a gif file named "trippy.gif". No other information is given.

## Inital Forensics

Let's start by opening it. 

![pic](tjctf_18/misc/Trippy/img/tripp1.png)




Not much information is gained from this. 


## Strings

Let's try running strings on the file and grepping for the flag format.

![pic](tjctf_18/misc/Trippy/img/tripp2.png)


There we go, that's the flag.


```
tjctf{w0w}

```