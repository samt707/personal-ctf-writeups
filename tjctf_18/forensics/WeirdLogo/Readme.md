# TJCTF 2018- Weird Logo (Forensics, 5 Points)

## Overview 

We are given a .png file with the following text:

```
This company's logo stands in contrast of those of other leading edge tech companies with its poor design.
```

## Initial Forensics


Let's start by just looking at the picture.

![pic](tjctf_18/forensics/WeirdLogo/img/logo1.png)


Nothing really to gain from this. Let's try running Strings on the image...

![pic](tjctf_18/forensics/WeirdLogo/img/logo2.png)

Nothing interesting here. Maybe it's a steg problem.


## Stegsolve

![pic](tjctf_18/forensics/WeirdLogo/img/logo3.png)

Start going through all the different presets in stegsolve. Once you get to "Red Plane 1", you will see the flag.

![pic](tjctf_18/forensics/WeirdLogo/img/logo4.png)





```
tjctf{in_plain_sight}
```


