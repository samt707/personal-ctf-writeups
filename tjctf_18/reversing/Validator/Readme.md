# TJCTF 2018- Validator (Reversing, 30 Points)

## Overview

You are given a binary and told:

```
I found a flag validation program. Do what you want with it.
```

## Inital Forensics

Let's start by running file on the binary to see what we are working with. 

![file](tjctf_18/reversing/Validator/img/flag1.png)

Looks like we have a 32-bit ELF binary. When we try to run it, we figure out that it takes the flag as a command line argument and tells you whether if it's a valid flag or not. 

![file](tjctf_18/reversing/Validator/img/flag1.2.png)

Let's open up Binaryninja.



## Binaryninja



First thing we notice is that it looks like the flag is hard-coded in the binary, that's good news for us.

![file](tjctf_18/reversing/Validator/img/flag2.png)

The bad news is that it is not straight forward; it looks like later in the binary some of the characters get flipped around.

![file](tjctf_18/reversing/Validator/img/flag3.png)

This means we are probably better off running the binary through GDB and seeing what the final flag is, as opposed as trying to build it ourselves. 
Before we jump into GDB, let's see what kind of comparisons happen to ensure we provide the proper input to get to the right spot in the binary. 

![file](tjctf_18/reversing/Validator/img/flag4.png)

The first check seems to make sure that we run the binary with exactly one argument, and the second comparison checks whether out input is equal to 0x2b in length, or 43 characters.


Let's make some fake input and move to GDB.

![file](tjctf_18/reversing/Validator/img/flag5.png)


## GDB

When we get into GDB, let's disassemble the main function and see where we should set our breakpoints.

![file](tjctf_18/reversing/Validator/img/flag6.png)

We will set a breakpoint at 0x080485ba. We choose this address because it is after all the operations that occur on the flag, but before the call to strcmp so we can see the value of the flag in memory.

Let's run the program with our crafted input.

![file](tjctf_18/reversing/Validator/img/flag7.png)


When the program pauses at our breakpoint, we see the flag right there on the stack.

![file](tjctf_18/reversing/Validator/img/flag1.1.png)




Let's make sure it works.



![file](tjctf_18/reversing/Validator/img/flag8.png)



It worked!

```
tjctf{ju57_c4ll_m3_35r3v3r_60d_fr0m_n0w_0n}
```