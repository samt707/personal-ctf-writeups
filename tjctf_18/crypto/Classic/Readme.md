# TJCTF 2018- Classic (Crypto, 40 Points)

## Overview

You are given a .txt file and told:

```
My primes might be close in size but they're big enough that it shouldn't matter right? rsa.txt

```

## Initial Forensics

Let's cat the .txt file and see what's inside.

![file](tjctf_18/crypto/Classic/img/rs1.png)

We are given the public key (e), the modulus (n), and the ciphertext (c).

## RSA

Here is what the basics of RSA encryption and decrytpion look like:

![file](tjctf_18/crypto/Classic/img/rs2.png)

Since we know what the encryption and decrytpion schemes look like, we now know that we need to find the private key (d) in order to decrypt the ciphertext.


In order to do that, we must first calculate phi which is (p-1)*(q-1). 
Since the problem said that the primes are close in size, and the problem gave us n, and n=p*q, we should be able to factor the modulus (n) using alpetron or factordb.

![file](tjctf_18/crypto/Classic/img/rs3.png)

Factordb worked like a charm, now we have p and q. Let's throw what we have so far into python. 

## Python

![file](tjctf_18/crypto/Classic/img/rs4.png)

Now that we have all the values we need to find d in python, let's start thinking how we can do this. 


We know that d is chosen such that:

```
d * e == 1 modulo (p-1)(q-1)
```

So in order to solve for d, we have to use the extended Euclidean algorithm.

Since this is not a math lesson, I found the following implementation of the extended Euclidean algorithm in python:

```python
def egcd(a, b):
    x,y, u,v = 0,1, 1,0
    while a != 0:
        q, r = b//a, b%a
        m, n = x-u*q, y-v*q
        b,a, x,y, u,v = a,r, u,v, m,n
        gcd = b
    return gcd, x, y
```

We should have everything we need now to find d and decrypt the ciphertext. Let's put it all in one script.

![file](tjctf_18/crypto/Classic/img/rs5.png)

When we run the script, we get the flag!

![file](tjctf_18/crypto/Classic/img/rs6.png)


```
tjctf{1_l1ke_squares}
```