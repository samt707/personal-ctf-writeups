# TJCTF 2018- Vinegar (Crypto, 15 Points)

## Overview

You are given a .txt file and told the following:

```

I just wanted something more than a Caesar salad. Maybe I should order another one? vinegar.txt

```

## Initial Forensics

When we run cat and xxd on vinegar.txt, we get the following output:

![out](tjctf_18/crypto/Vinegar/img/vin1.png)

Not really sure what to do from here. Let's just see if we can look up the SHA-256 hash in a online dictionary and maybe we'll get lucky.


## SHA-256

![out](tjctf_18/crypto/Vinegar/img/vin2.png)


Wow...I can't believe we got that.



```
 tjctf{onevinaigrettesaladplease}
```