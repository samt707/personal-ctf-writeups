# TJCTF 2018- Central Savings Account (Web, 10 Points)

## Overview

You are given a link to a website and told:

```
I seem to have forgotten the password for my savings account. What am I gonna do?
```


## Inital Forensics

When we go to the website, we see a simple login screen. 

![webpage](tjctf_18/web/CentralSavingsAccount/img/saving1.png)

Let's take a look at the source.

![webpage](tjctf_18/web/CentralSavingsAccount/img/saving2.png)

So nothing really in the source. It looks like the password check happens in javascript. Let's start with main.js.



## Javascript

If you scroll down to the bottom of main.js, you see where your password is actually checked. Your input is put through a md5 hash function and then checked against a hard-coded hash.

![webpage](tjctf_18/web/CentralSavingsAccount/img/saving3.png)
```
698967f805dea9ea073d188d73ab7390
```

## md5
Let's see if we can look this hash up in an online md5 dictionary.

![webpage](tjctf_18/web/CentralSavingsAccount/img/saving4.png)

The dictionary returns "avalon".

Let's try putting that in as the password.



![webpage](tjctf_18/web/CentralSavingsAccount/img/saving5.png)



It works! That is the flag for this problem.


```
avalon
```
