# TJCTF 2018- Blank (Web, 5 Points)

## Overview

You are given the following statement:

```
Someone told me there was a flag on this site, so why is it that I can only see blank?
```

## Initial Forensics

When you go to the site, you don't see any text. All you see is two shapes in the middle of the screen. 

![webpage](tjctf_18/web/Blank/img/blank1.png)


Let's take a look at the source. 


## Source Code

When you go to the source by right-clicking and going to "View page source".


![webpage](tjctf_18/web/Blank/img/blank2.png)


You see the flag in the source as a comment.


![webpage](tjctf_18/web/Blank/img/blank3.png)


```
tjctf{50urc3_c0d3_n3v3r_l0535}
```
