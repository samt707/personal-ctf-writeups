# TJCTF 2018- Cookie Monster (Web, 10 Points)

## Overview

You are given a website and told:

```
The Cookie Monster is not a fan of horses.

```

Let's start by going to the webpage.

![webpage](tjctf_18/web/CookieMonster/img/cookie1.png)



Looks like a horse with very long legs...interesting. Let's look at the source.

![webpage](tjctf_18/web/CookieMonster/img/cookie2.png)


Nothing in the source. Judging by the title, we should probably see if the website sets any cookies.


## Cookies
Right-click on the webpage and go to "Inspect".

![webpage](tjctf_18/web/CookieMonster/img/cookie3.png)


From there, go to "Application" and then select the cookie from the current website.

![webpage](tjctf_18/web/CookieMonster/img/cookie4.png)


And that's it. The flag is the value of the cookie 'flag' that the website set.


```
tjctf{c00ki3s_over_h0rs3s}
```






